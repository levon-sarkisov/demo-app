package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;

@Controller
@RequestMapping("/")
public class DemoController {

    @GetMapping("demo")
    public String home() {
        // File newFile = new File("/home/zero/Desktop/Hello1.txt");
        return "home";
    }
}
